from colorama import Fore, Back, init
from settings import *

import random

class Coins:

    def __init__(self):

        self.angle = random.randint(0,2)
        self.shape = [ [ BACKGROUND for i in range(COIN_GRID_SIZE) ] for j in range(COIN_GRID_SIZE) ]

        # print(self.angle)

        if self.angle == 0:
            for i in range(COIN_GRID_SIZE):
                for j in range(2):
                    self.shape[i][j] = COIN

        elif self.angle == 1: 
            for i in range(2):
                for j in range(COIN_GRID_SIZE):
                    self.shape[i][j] = COIN

        elif self.angle == 2:
            for i in range(COIN_GRID_SIZE):
                for j in range(COIN_GRID_SIZE):
                    self.shape[i][j] = COIN

    def place(self, x, board):

        self.x = random.randint(x, x+4)
        
        if self.angle == 2:
            self.y = 29-COIN_GRID_SIZE
        else:
            self.y = random.randint(0, ROWS - COIN_GRID_SIZE - 12)

        for i in range(COIN_GRID_SIZE):
            for j in range(COIN_GRID_SIZE):
                try:
                    board.change(self.y + j, self.x + i, self.shape[i][j])
                except:
                    with open("log.txt", "w") as log:
                        log.write(str(self.y + j))
                        log.write("\n")
                        log.write(str(self.x + i))
                        log.write("\n")
                        log.write(str(j))
                        log.write("\n")
                        log.write(str(i))