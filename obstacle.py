from colorama import Fore, Back, init
from settings import *

from object_board import *

import random

class FireBeam(Object):

    def __init__(self):

        self.__is_hit = False
        self.angle = random.randint(0,2)
        self.shape = [ [ BACKGROUND for i in range(LASER_GRID_SIZE) ] for j in range(LASER_GRID_SIZE) ]

        if self.angle == 0:
            for i in range(LASER_GRID_SIZE):
                for j in range(2):
                    self.shape[i][j] = LASER

        elif self.angle == 1: 
            for i in range(2):
                for j in range(LASER_GRID_SIZE):
                    self.shape[i][j] = LASER

        elif self.angle == 2:
            for i in range(LASER_GRID_SIZE):
                for j in range(LASER_GRID_SIZE):
                    if i == j:
                        self.shape[i][j] = LASER

    def place(self, x, board):

        self.__x = random.randint(x, x+4)
        
        if self.angle == 2:
            self.__y = 29-LASER_GRID_SIZE
        else:
            self.__y = random.randint(0, ROWS - LASER_GRID_SIZE - 12)

        for i in range(LASER_GRID_SIZE):
            for j in range(LASER_GRID_SIZE):
                try:
                    board.change(self.__y + j, self.__x + i, self.shape[i][j])
                except:
                    pass

    def remove_laser(self, board):
        for i in range(LASER_GRID_SIZE):
            for j in range(LASER_GRID_SIZE):
                board.change(self.__y + j, self.__x + i, COIN)

    def is_hit(self, board):
        for i in range(LASER_GRID_SIZE):
            for j in range(LASER_GRID_SIZE):
                for k in range(2):
                    for l in range(2):
                        try:
                            if board.return_char(self.__y + j + k, self.__x + i + l) in [ 'X', '-', '-', '>' ]:
                                # print("HIT\n")
                                self.__is_hit = True
                                self.remove_laser(board)
                                break
                        except:
                            # print(self.__y + j + k, self.__x + i + l)
                            pass

class Magnet(Object):

    def __init__(self):
        self.__shape = [ [ BACKGROUND for i in range(LASER_GRID_SIZE) ] for j in range(LASER_GRID_SIZE) ]

        for i in range(MAGNET_GRID_SIZE):
            for j in range(3):
                if i<=1:
                    self.__shape[i][j] = MAGNET_ALTER
                else:
                    self.__shape[i][j] = MAGNET
            for j in range(MAGNET_GRID_SIZE - 3, MAGNET_GRID_SIZE):
                if i<=1:
                    self.__shape[i][j] = MAGNET_ALTER
                else:
                    self.__shape[i][j] = MAGNET

        for i in range(MAGNET_GRID_SIZE - 3, MAGNET_GRID_SIZE):
            for j in range(MAGNET_GRID_SIZE):
                self.__shape[i][j] = MAGNET

    def place(self, x, board):

        self.set_x( random.randint(x, x+10) )
        
        self.set_y( random.randint(0, ROWS - MAGNET_GRID_SIZE - 5) )

        for i in range(MAGNET_GRID_SIZE):
            for j in range(MAGNET_GRID_SIZE):
                try:
                    board.change(self.return_y() + j, self.return_x() + i, self.__shape[i][j])
                except:
                    pass

    def update_position(self, board):
        for i in range(MAGNET_GRID_SIZE):
            for j in range(MAGNET_GRID_SIZE):
                try:
                    board.change(self.return_y() + j, self.return_x() + i, self.__shape[i][j])
                except:
                    pass
