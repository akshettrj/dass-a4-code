from settings import *
from object_board import *

import random

class SpeedBoost(Object):
    def __init__(self):
        self.__activate = False
        self.shape = [ [ SPEED_BOOST for i in range(SPEED_BOOST_GRID_SIZE) ] for j in range(SPEED_BOOST_GRID_SIZE) ]

    def place(self, board):

        not_free = True

        while not_free:

            not_free = False

            x = self.__x = random.randint(6,18)
            y = self.__y = random.randint(40, 200)

            for i in range(SPEED_BOOST_GRID_SIZE):
                for j in range(SPEED_BOOST_GRID_SIZE):
                    try:
                        if(board.return_char(self.y + j, self.x + i, self.shape[i][j]) != BACKGROUND):
                            not_free = True
                    except:
                        break

        print(self.__x, self.__y)

        for i in range(SPEED_BOOST_GRID_SIZE):
            for j in range(SPEED_BOOST_GRID_SIZE):
                try:
                    board.change(self.__x + i, self.__y + j, self.shape[i][j])
                except:
                    print(self.__y + j, self.__x + i)
    
    def speed_activated(self):
        if not self.__activate:
            return BACKGROUND_STEP_SIZE
        else:
            return 2

    def activate(self, board):
        self.__activate = True
        for i in range(SPEED_BOOST_GRID_SIZE):
            for j in range(SPEED_BOOST_GRID_SIZE):
                board.change(self.__x + i, self.__y + j, BACKGROUND)