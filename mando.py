from object_board import *
from colorama import Fore, Back, init

from settings import *
from character import *

import time

init()

class Mando(Object, Character):

    def __init__(self, x, y):
        Object.__init__(self, x, y)

        self.__shape = [

            [ BACKGROUND , HEAD, BACKGROUND ],
            
            [ JETPACK, BODY , HAND],

            [ BACKGROUND, LEG, BACKGROUND]
            
            ]

        self.__shield_shape = [
            [ SHIELD, SHIELD, SHIELD ],
            [ SHIELD, HEAD, SHIELD ],
            [ SHIELD, SHIELD, SHIELD ]
         ]

        self.__dim = [3, 3]
        self.__shield = False
        self.__start_time = 0
        self.__velocity_y = 0

    def check_valid(self, board):
        pass
    
    def start_position(self, board):
        ''' Set mandalorian to intial position on the board'''
        
        for i in range(0, 3):
            for j in range(0, 3):
                board.change(ROWS - 3 + i, j, self.__shape[i][j])

    def remove_mando(self, board):
        ''' Remove the mandalorian for repositioning '''

        for i in range(0,3):
            for j in range(0,3):
                board.change(ROWS - 3 - self.return_y() + i, self.return_x() + j, BACKGROUND)
    
    def update_position(self, board):
        ''' Update Position of mandalorian '''
        
        if time.time() - self.get_start_time() > 10:
            self.deactivate_shield()

        for i in range(0, 3):
            for j in range(0, 3):
                if self.__shield:
                    board.change(ROWS - 3 - self.return_y() + i, self.return_x() + j, self.__shield_shape[i][j])
                else:
                    board.change(ROWS - 3 - self.return_y() + i, self.return_x() + j, self.__shape[i][j])

    def activate_shield(self):
        
        if time.time() - self.get_start_time() > 60:
            self.__shield = True
            self.__start_time = time.time()

    def deactivate_shield(self):

        self.__shield = False

    def attract_left(self):
        self.set_x(self.return_x() - ATTRACT_LEFT)
        pass

    def attract_right(self):
        self.set_x(self.return_x() + ATTRACT_RIGHT)
        pass

    def push_forward(self):
        self.set_x( self.return_x() + 1 )

    def move_right(self):
        self.set_x(self.return_x() + RIGHT_STEP)

    def move_left(self):
        self.set_x(self.return_x() - LEFT_STEP)

    def move_down(self):
        self.set_y(self.return_y() - 1)

    def move_up(self):
        self.__velocity_y = FLY_DIST
        self.set_y(self.return_y() + FLY_DIST)

    def update_velocity(self):
        # print("Velocity", self.__velocity_y)
        if self.__velocity_y == FLY_DIST:
            self.__velocity_y = 1
        elif self.__velocity_y >= 4:
            self.__velocity_y = 4
        else:
            self.__velocity_y += 0.75*self.__velocity_y
            self.__velocity_y = round(self.__velocity_y)

    def set_velocity(self, val):
        self.__velocity_y = val

    def return_velocity(self):
        return self.__velocity_y

    def return_corners(self):

        return [[self.x, ROWS - 1 - self.y], [self.x + 3 - 1, ROWS - 1 - self.y], [self.x, ROWS - 1 -
            self.y - 2], [self.x + 3 - 1 , ROWS - 1 - self.y - 2]]

    def get_start_time(self):

        return self.__start_time

    def is_shield_active(self):

        return self.__shield
