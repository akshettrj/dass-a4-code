import os
import time
import emoji

from board import Board
from mando import Mando
from getch import AlarmException
from getch import _getChUnix as getChar
from colorama import Fore, Back, init
from settings import *
from obstacle import *
from coins import *
from powerup import *
from bullet import *
from dragon import *

init()

import signal

class Game:

    def __init__(self):
        '''Initializes the board for the game and other attributes of the game'''

        self.__score = 0
        self.__life = 10
        self.__enemy_life = 5
        self.__time = time.time()
        self.__max_time = 240
        self.obj_board = Board()
        self.obj_board.create_board()
        self.dragon = Dragon()

    def get_score(self):
        return self.__score

    def get_life(self):
        return self.__life

    def set_score(self, inc):
        self.__score += inc

    def set_life(self, inc):

        os.system('clear')

        for i in range(ROWS):
            for j in range(100):
                if inc < 0:
                    print(Back.RED + " " + Back.RESET, end='')
                else:
                    print(Back.GREEN + " " + Back.RESET, end='')
            print()

        time.sleep(0.25)

        self.__life += inc

        os.system('clear')

    def print_score(self):

        if self.get_score() < 10:
            print(Fore.YELLOW + "Score: " + Fore.RESET + "00" + str(self.get_score()))
        elif self.get_score() < 100:
            print(Fore.YELLOW + "Score: " + Fore.RESET + "0" + str(self.get_score()))
        else:
            print(Fore.YELLOW + "Score: " + Fore.RESET + str(self.get_score()))
        print(Fore.YELLOW + "Lives Remaining: " + Fore.RESET + str(self.get_life()) )
        print(Fore.YELLOW + "Time Remaining: " + Fore.RESET + str(round(self.__max_time -
            time.time() + self.__time)) + " Seconds")
        print()

    def coin_hit(self):

        self.set_score(1)

    def life_lost(self, mando):
        
        if not mando.is_shield_active():
            self.set_life(-1)

            if self.obj_board.return_start() <= (COLUMNS - 200):
                mando.move_right()
                mando.move_right()

    def continue_game(self):

        if self.get_life() <= 0:
            return False

        if time.time() - self.__time >= self.__max_time:
            return False

        return True

    def end(self):

        os.system('clear')

        for j in range(4):
            self.obj_board.change(ROWS - 3, 660 + j, YODA_HEAD[j])
            self.obj_board.change(ROWS - 2, 660 + j, YODA_BODY[j])

        self.obj_board.print_board(COLUMNS, True)

        if self.get_life() > 0 and not self.dragon.return_is_alive():
            print(Fore.YELLOW + "Congrats! This is the Way!" + Fore.RESET)
        else:
            print(Fore.YELLOW + "This is not the way!" + Fore.RESET)
        
        print(Fore.YELLOW + "Score: " + Fore.RESET + str(self.get_score()))

    def start(self):

        def set_cursor_top():
            print("\033[0;0H")

        def alarmhandler(signum, frame):
            ''' input method '''
            raise AlarmException

        def user_input(timeout=0.1):
            ''' input method '''
            signal.signal(signal.SIGALRM, alarmhandler)
            signal.setitimer(signal.ITIMER_REAL, timeout)
            
            try:
                text = getChar()()
                signal.alarm(0)
                return text
            except AlarmException:
                pass
            signal.signal(signal.SIGALRM, signal.SIG_IGN)
            return ''

        def check_valid_position(x, y):

            pos_x = mando.return_x()
            pos_y = mando.return_y()
            acceptable_list = [BACKGROUND, COIN, SPEED_BOOST]


            start = self.obj_board.return_start() 

            if (pos_x + x) < start :
                return False

            if (pos_x + x) >= (start + 97):
                return False

            if x>0:

                if (pos_x + RIGHT_STEP + 2)>=COLUMNS:
                    return False
                
                for i in range(3):
                    if self.obj_board.return_char(ROWS - pos_y - 3 + i, pos_x + RIGHT_STEP +2) not in acceptable_list:
                        self.life_lost(mando)
                        return False
                    elif self.obj_board.return_char(ROWS - pos_y - 3 + i, pos_x + RIGHT_STEP +2) == SPEED_BOOST:
                        speedboost.activate(self.obj_board)
                    elif self.obj_board.return_char(ROWS - pos_y - 3 + i, pos_x + RIGHT_STEP +2) == COIN:
                        self.coin_hit()

            elif x<0:

                if (pos_x - LEFT_STEP)<0:
                    return False

                for i in range(3):
                    if self.obj_board.return_char(ROWS - pos_y - 3 + i, pos_x - LEFT_STEP) not in acceptable_list:
                        self.life_lost(mando)
                        return False
                    elif self.obj_board.return_char(ROWS - pos_y - 3 + i, pos_x - LEFT_STEP) == SPEED_BOOST:
                        speedboost.activate(self.obj_board)
                    elif self.obj_board.return_char(ROWS - pos_y - 3 + i, pos_x - LEFT_STEP) == COIN:
                        self.coin_hit()

            elif y<0:

                if (ROWS - pos_y - 3 - FLY_DIST) <0:
                    return False

                for i in range(3):
                    if self.obj_board.return_char(ROWS - pos_y - 3 - FLY_DIST, pos_x + i) not in acceptable_list:
                        self.life_lost(mando)
                        return False
                    elif self.obj_board.return_char(ROWS - pos_y - 3 - FLY_DIST, pos_x + i) == SPEED_BOOST:
                        speedboost.activate(self.obj_board)
                    elif self.obj_board.return_char(ROWS - pos_y - 3 - FLY_DIST , pos_x + i) == COIN:
                        self.coin_hit()

            elif y>0:
                
                if pos_y == 0:
                    return False

                for i in range(3):
                    if self.obj_board.return_char(ROWS - pos_y, pos_x + i) not in acceptable_list:
                        self.life_lost(mando)
                        return False
                    elif self.obj_board.return_char(ROWS - pos_y, pos_x + i) == SPEED_BOOST:
                        speedboost.activate(self.obj_board)
                    elif self.obj_board.return_char(ROWS - pos_y, pos_x + i) == COIN:
                        self.coin_hit()

            return True

        def gravity_pull(mando):

            mando.update_velocity()

            if mando.return_y() == 0:
                mando.set_velocity(0)

            mando.remove_mando(self.obj_board)

            for val in range(mando.return_velocity()):
                is_valid = check_valid_position(0, 1)
                if is_valid:
                    mando.move_down()

            mando.update_position(self.obj_board)

        mando = Mando(0, 0)
        mando.start_position(self.obj_board)

        lasers = []

        for i in range(14):
            lasers.append(FireBeam())
            lasers[i].place(40 * (i+1), self.obj_board)

        coins = []

        for i in range(22):
            coins.append(Coins())
            coins[i].place(25 * (i+1), self.obj_board)

        magnet = Magnet()
        magnet.place(410, self.obj_board)

        speedboost = SpeedBoost()
        speedboost.place(self.obj_board)

        bullets = []

        dragon = self.dragon
        dragon.place(self.obj_board)

        iceballs = []

        removed = []

        os.system('clear')

        while self.continue_game():
            
            # Reset cursor to the start of the terminal
            set_cursor_top()

            # Print updated terminal
            self.print_score()
            self.obj_board.print_board(speedboost.speed_activated())

            # Accept User Input
            char = user_input()

            if char == 'q' or char == 'Q':
                break
        
            # Simulate gravity and pull the mando down
            gravity_pull(mando)

            mando.remove_mando(self.obj_board)

            if magnet.return_x() > mando.return_x() and (magnet.return_x() - mando.return_x()) <=25:
                for value in range(ATTRACT_RIGHT, 0, -1):
                    is_valid = check_valid_position(ATTRACT_RIGHT,0)
                    if is_valid:
                        mando.attract_right()
                        break

            if magnet.return_x() < mando.return_x() and (mando.return_x() - magnet.return_x()) <=25:
                for value in range(ATTRACT_LEFT, 0, -1):
                    is_valid = check_valid_position(-ATTRACT_LEFT,0)
                    if is_valid:
                        mando.attract_left()
                        break

            if char == 'd' or char == 'D':
                for value in range(RIGHT_DIST, 1, -1):
                    is_valid = check_valid_position(value,0)
                    if is_valid:
                        mando.move_right()
                        break
                        
            elif char == 'a' or char == 'A':
                for value in range(LEFT_DIST, 1, -1):
                    is_valid = check_valid_position(-value, 0)
                    if is_valid:
                        mando.move_left()
                        break

            elif char == 'w' or char == 'W':
                for value in range(FLY_DIST, 0, -1):
                    is_valid = check_valid_position(0, -value)
                    if is_valid:
                        mando.move_up()
            
            elif char == ' ':

                mando.activate_shield()

            elif char == 'b' or char == 'B':
                
                bullets.append(Bullet(mando))

            elif char == 'l' or char =='L':

                self.set_life(1)

            if (mando.return_x() - self.obj_board.return_start()) <=2:
                is_valid = check_valid_position(1,0)
                if is_valid:
                    mando.move_right()

            mando.update_position(self.obj_board)

            dragon.remove(self.obj_board)

            mando_y = mando.return_y()

            dragon.is_hit(self.obj_board)

            if dragon.return_is_alive():
                drag_pos = ROWS - 1 - 15 - mando_y

                if drag_pos < 0:
                    drag_pos = 0

                dragon.set_y(drag_pos)

                dragon.place(self.obj_board)

                if dragon.is_shoot():
                    iceballs.append(IceBall(dragon))
            else:
                break
                
            iceballs = [ iceball for iceball in iceballs if iceball.is_active_ice() ]
            
            for iceball in iceballs:
                if iceball.is_active_ice():
                    iceball.remove_bullet(self.obj_board)
                    iceball.move()
                if iceball.is_active_ice():
                    iceball.update_position(self.obj_board)

            # Update positions for all bullets shot so far
            for bullet in bullets:
                if bullet.is_active():
                    bullet.remove_bullet(self.obj_board)
                    bullet.move()
                if bullet.is_active():
                    bullet.update_position(self.obj_board)

            for laser in lasers:
                laser.is_hit(self.obj_board)

            magnet.update_position(self.obj_board)

        for iceball in iceballs:
            iceball.remove_bullet(self.obj_board)
        for bullet in bullets:
            bullet.remove_bullet(self.obj_board)

game = Game()
game.start()
game.end()
