from settings import *
from character import *
from object_board import *
from bullet import *
from colorama import Fore, Back, init

import time

init()

class Dragon(Character, Object):

    def __init__(self):
        Object.__init__(self, 660, 10)
        self.__shape = []
        with open("./Objects/viserion.txt") as obj:
            for line in obj:
                self.__shape.append(line.strip('\n'))

        self.__last_shoot = 0
        self.__lives = 100
        self.__last_time = time.time()

    def is_shoot(self):

        if time.time() - self.__last_shoot > 0.5:
            self.__last_shoot = time.time()
            return True

        return False

    def remove(self, board):
        for i in range(15):
            for j in range(39):
                try:
                    board.change(self.return_y() + i, self.return_x() +j, BACKGROUND)
                except:
                    print(self.return_y() + i, self.return_x() + j)   
                    pass

    def place(self, board):

        for i in range(15):
            for j in range(39):
                try:
                    board.change(self.return_y() + i, self.return_x() +j, Back.WHITE + Fore.BLUE + self.__shape[i][j] + Fore.RESET + Back.RESET)     
                except:
                    # print(self.return_y() + i, self.return_x() +j)
                    pass

    def is_hit(self, board):
        for i in range(15):
            for j in range(39):
                for k in range(2):
                    for l in range(2):
                        try:
                            if board.return_char(self.return_y() + i + k, self.return_x() + j - l) in [ 'X', '-', '-', '>' ]:
                                self.__lives -= 1
                                if self.__lives <= 0:
                                    self.remove(board)
                                break
                        except:
                            pass
 
    def return_life_count(self):
        return self.__lives

    def return_is_alive(self):
        if self.__lives <= 0:
            return False
        return True

class IceBall(Bullet):

    def __init__(self, dragon):
        Bullet.__init__(self, dragon)
        self.set_shape( [ICEBALL] )
        self.set_initial(dragon.return_x(), ROWS - 10 - dragon.return_y() )
        self.set_active(True)
        self.set_direction(-4)

    def is_active_ice(self):

        if (self.return_x() >= (COLUMNS - 2)):
            self.set_active(False)

        if (self.return_x() - self.return_initial_x()) <= -70:
            self.set_active(False)

        return self.return_active()
    
    def update_position(self, board):

        ''' Update Position of bullet '''
        if self.is_active_ice():
            for i in range(0, 1):
                for j in range(0, 4):
                    try:
                        board.change(ROWS - 1 - self.return_y() + i, self.return_x() + j,
                                self.return_shape(i,j))
                    except:
                        pass
