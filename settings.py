from colorama import Fore, Back, init, Style
import emoji

init()

ROWS = 30
COLUMNS = 700
FLY_DIST = 3
LEFT_DIST = 2
RIGHT_DIST = 2

BACKGROUND = Back.WHITE + " " + Back.RESET + Style.RESET_ALL
BACKGROUND_STEP_SIZE = 1

BACKGROUND_OUTER = Back.CYAN + "-" + Back.RESET

HEAD = Back.WHITE + Fore.BLUE + '\u234C' + Back.RESET + Fore.RESET
JETPACK = Back.BLUE + Fore.YELLOW + "\u168F" + Fore.RESET +  Back.RESET
BODY = Back.WHITE + Fore.BLUE + '\u2588' + Fore.RESET + Back.RESET
HAND = Back.WHITE + Fore.BLUE + '√' + Fore.RESET + Back.RESET
LEG = Back.WHITE + Fore.BLUE + '\u2588' + Fore.RESET +  Back.RESET
SHIELD = Back.GREEN + ' ' + Back.RESET

RIGHT_STEP = 3
LEFT_STEP = 3

COIN = Back.WHITE + Fore.YELLOW + "©" + Fore.RESET + Back.RESET
COIN_GRID_SIZE = 4

WINDOW_SIZE = 100
BOSS_START = 400

LASER = Back.RED + Fore.RED + '#' + Fore.RESET + Back.RESET
LASER_GRID_SIZE = 12

MAGNET = Back.MAGENTA + ' ' + Back.RESET
MAGNET_ALTER = ' '
MAGNET_GRID_SIZE = 8
ATTRACT_LEFT = 2
ATTRACT_RIGHT = 2


SPEED_BOOST_GRID_SIZE = 5
SPEED_BOOST = Back.CYAN + ' ' + Back.RESET

BULLET = [ 'X', '-', '-', '>' ]

DRAGON_POSITION_X = 650
DRAGON_POSITION_Y = 15

ICEBALL = [ Back.BLUE + Fore.WHITE + "O" + Fore.RESET + Back.RESET ] * 4

YODA_HEAD = [ Back.WHITE + Fore.GREEN + '<' + Fore.RESET + Back.RESET, Back.WHITE + Fore.GREEN + '•'
        + Fore.RESET + Back.RESET, Back.WHITE + Fore.GREEN + '•' +
        Fore.RESET + Back.RESET, Back.WHITE + Fore.GREEN + '>' + Fore.RESET + Back.RESET ]
YODA_BODY = [ Back.WHITE + Fore.GREEN + '(' + Fore.RESET + Back.RESET, Back.YELLOW + " " +
        Back.RESET, Back.YELLOW + " " + Back.RESET, Back.WHITE + Fore.GREEN + ')' + Fore.RESET +
Back.RESET ]
