import numpy as np
import emoji
from colorama import init, Fore, Back
from settings import *

init()

class Board:
    '''
    This class sets up the board and draws everything on the terminal
    '''

    def __init__(self):
        self.__rows = ROWS
        self.__columns = COLUMNS
        self.__matrix = []
        self.__start = 0

    def create_board(self):
        self.__matrix = [ [ BACKGROUND for j in range(self.__columns)] for i in range(self.__rows) ]

    def return_start(self):
        return self.__start

    def print_board(self, speed=BACKGROUND_STEP_SIZE, end=False):

        if self.__start >= (COLUMNS - 100 - 1) or end:
            self.__start = COLUMNS - 101

        for i in range(2):
            for j in range(100):
                print(BACKGROUND_OUTER, end='')
            print()

        for i in range(self.__rows):
            for j in range(self.__start, self.__start+100):
                print(self.__matrix[i][j], end='')
            print()

        for i in range(2):
            for j in range(100):
                print(BACKGROUND_OUTER, end='')
            print()

        self.__start += speed

    def change(self, x, y, char):
        try:
            self.__matrix[x][y] = char
        except:
            print(x, y, char)

    def return_char(self, x, y):
        return self.__matrix[x][y]
