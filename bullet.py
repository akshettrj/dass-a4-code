from object_board import *
from colorama import Fore, Back, init

from settings import *

init()

class Bullet(Object):

    def __init__(self, char):
        Object.__init__(self, char.return_x() + 1, char.return_y() + 2)

        self.__shape = [BULLET]
        self.__initial_x = char.return_x() + 3
        self.__initial_y = char.return_y()
        self.__active = True
        self.__direction = 4

    def move(self):

        self.set_x(self.return_x() + self.__direction)
    
    def is_active(self):

        if self.return_x() >= (COLUMNS - 4):
            self.__active = False

        if (self.return_x() - self.__initial_x) >=40:
            self.__active = False

        return self.__active

    def remove_bullet(self, board):
        ''' Remove the bullet for repositioning '''

        for i in range(0,1):
            for j in range(0,4):
                try:
                    board.change(ROWS - 1 - self.return_y() + i, self.return_x() + j, BACKGROUND)
                except:
                    print("remove ", ROWS - 1 - self.return_y() + i, self.return_x() + j)
    
    def update_position(self, board):

        ''' Update Position of bullet '''
        if self.is_active():
            for i in range(0, 1):
                for j in range(0, 4):
                    try:
                        board.change(ROWS - 1 - self.return_y() + i, self.return_x() + j, self.__shape[i][j])
                    except:
                        print("update ", ROWS - 1 - self.return_y() + i, self.return_x() + j)

    def set_direction(self, val):
        self.__direction = val

    def set_active(self, val):
        self.__active = val

    def set_initial(self, x, y):
        self.set_x(x)
        self.set_y(y)
        self.__intial_x = x
        self.__initial_y = y

    def set_shape(self, val):
        self.__shape = val

    def return_initial_x(self):
        return self.__initial_x

    def return_active(self):
        return self.__active

    def return_shape(self, i, j):
        return self.__shape[i][j]
