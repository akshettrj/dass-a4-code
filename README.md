# Jetpack Joyride

This is a pure terminal emulation of the popular game Jetpack Joyride

## Installation and Run

```
pip3 install -r requirements.txt
bash run.sh
```

## Settings.py

This file contains some crucial variables for the game, that are used across multiple files.
Editing the variables in this file will change the appearance and behaviour of the game. 
Modify with care

## How to Play

W - Fly
A - Left
D - Right
Space - Shield Activation (60 sec recharge)
B - Shoot Bullets
L - Additional Life Cheat

Q - quit the game

The objective of the game is to defeat the final boss - a dragon and save baby yoda. Collect coins
along the way to boost your score

## Special Features

1. The lasers can be destroyed when shot at! Convert lasers to a massive coin stash by shooting at
   them.
2. Magnetic Attraction! A magnet randomly spawns and makes moving harder by attracting you towards
   it!
3. Bullets don't last forever. To make the game challenging, the bullets disappear after travelling
   for a few blocks. You can't have it that easy!
4. Dragon Sheild: It would be unfair if you hid under the dragon and shot at it. So, the dragon has
   a force field which reduces life if you touch it.

## Object Oriented Programming

1. board.py (Board) : This is the most important class of all and contains the actual game, and renders the
   game
2. main.py (Game) : This brings together all the components and manages the logic of the game. This
   takes the input, and handles instances of all the other classes.
3. character.py, object_board.py : These are high level classes that are inherited from by other
   classes to provide standard funcionlity based on life, position, etc.
4. powerup.py : Manages all the powerups of the game
5. obstacle.py: Manages the laser beams, and the magnet
6. dragon.py: The almighty villain! The class to end the game!
7. coins.py: Describes a cluster of coins (randomized shapes)
8. bullet.py: Describes the bullet class which is used by mandolorian and the dragon
9. mando.py: Din the mandalorian! This class manages the movement of the mandalorian
