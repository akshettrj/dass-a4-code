class Character:

    def __init__(self):

        self.__lives = 5

    def return_lives(self):

        return self.__lives

    def reduce_life(self):

        self.__lives -= 1